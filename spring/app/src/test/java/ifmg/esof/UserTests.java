package ifmg.esof;

import ifmg.esof.model.User;
import ifmg.esof.repository.UserRepository;
import ifmg.esof.services.UserService;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserTests {

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserService service;

    @Test
    public void shouldCreateEmptyUser() {
        User foo = new User();

        Long id = service.createUser(foo);
        User _foo = service.getUser(id);

        assertEquals(foo.getEmail(), _foo.getEmail());
    }

    @Test
    public void shouldDeleteUserById() {
        User foo = new User();

        Long id = service.createUser(foo);

        Long count = repository.count();

        service.deleteUser(id);

        assertEquals(repository.count(), count - 1);
    }

    @Test
    public void shouldDeleteAllUsers() {
        User foo = new User();

        Long id = repository.count();

        repository.save(foo);
        service.deleteAllUsers();

        assertEquals(repository.count(), 0);
    }

    @Test
    public void shouldUpdateSpecificUser() {
        User foo = new User();

        Long id = service.createUser(foo);

        foo.setEmail("baz@gmail.com");
        foo.setPassword("12345678");

        service.updateUser(id, foo);

        assertEquals(service.getUser(id).getEmail(), foo.getEmail());
        assertEquals(service.getUser(id).getPassword(), foo.getPassword());
    }
}
