package ifmg.esof.services;

import ifmg.esof.model.User;
import ifmg.esof.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public Long createUser(User user) {
        User _user = userRepository.save(user);

        return _user.getId();
    }

    public void updateUser(Long id, User user) {
        User _user = userRepository.getById(id);
        _user.setEmail(user.getEmail());
        _user.setPassword(user.getPassword());
        userRepository.save(_user);
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    public void deleteAllUsers() {
        userRepository.deleteAll();
    }

    public User getUser(Long id) {
        return userRepository.getById(id);
    }
}
